var timer;

function setActionTimeout(element, milisec)
{
    timer = setTimeout(() => {
        let closeBut = element.getElementsByTagName('button')[0];
        closeBut.style.display = 'block';
    }, milisec);
}

export class HoldDown
{   
    static add_Listener(element)
    {
        if(this.isTouchDevice())
        {
            element.ontouchstart = () => setActionTimeout(element, 2000);
            element.ontouchend = () => clearTimeout(timer);
        }
        else
        {
            element.onmousedown = () => setActionTimeout(element, 2000);
            element.onmouseup = () => clearTimeout(timer);
        }
    }

    static isTouchDevice()
    {
        return (('ontouchstart' in window) ||
           (navigator.maxTouchPoints > 0) ||
           (navigator.msMaxTouchPoints > 0));
    }
};