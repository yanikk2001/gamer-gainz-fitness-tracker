export const URL = {
    ADD_EXER: '/add-exercise',
    ADD_EXISTING_EXER: '/add-existing-exercise/',
    DAY: '/day/',
    ICON_EXER: '/media/assets/image-fill.svg',
    GET_EXER_LIST: '/list-exercises',
    GET_EXER_LOG: '/exer-log/', 
    ADD_EXER_LOG: '/add-log',
    SHOW_DAY: '/Day/',
    DELETE_EXER: '/delete-exercise/',
};