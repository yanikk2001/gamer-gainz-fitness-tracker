from django.http.response import HttpResponseRedirect, JsonResponse
import json
from django.shortcuts import render, HttpResponsePermanentRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib import messages

from .forms import ExerciseForm, UserLoginForm, UserRegisterForm, DayForm, LogForm
from .models import *
from .views_helper import *

@login_required
def index(request):
    return render(request, 'Ledger/index.html')

@login_required
def profileView(request):
    return render(request, 'Ledger/profile.html')

@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("login"))

@login_required
def calendarView(request):
    return render(request, 'Ledger/calendar.html')

@login_required
def day(request, day_number):
    return render(request, 'Ledger/index.html', {
        "dayNumber": day_number
    })

@login_required
def getDays(request):
    #Verify request method
    if request.method != 'GET':
        return JsonResponse({"error": "GET request required"}, status=400)

    # Get start and end points
    start = int(request.GET.get("start") or 1)
    end = int(request.GET.get("end") or (start + 9))

    # Generate list of days
    data = []
    for i in range(start, end + 1):
        try:
            day = getDay(i, request.user)
            day = day.calSerialize()
        except Day.DoesNotExist:
            continue
        data.append(day)

    # Return list of days
    return JsonResponse({
        "days": data
    })
        
@login_required
def dayApi(request, day_number):
    #Verify request method
    if request.method != 'GET':
        return JsonResponse({"error": "GET request required"}, status=400)

    if day_number < 1 : #Day validity check
        return JsonResponse({"error": "Invalid day number"}, status=400)
    
    #Get day
    try:
        day = getDay(day_number, request.user)
    except Day.DoesNotExist:
        return JsonResponse({"error": "Day not found"}, status=204)
    
    # if day.exercises_count == 0:
    #     return JsonResponse({"error": "No exercises added yet"}, status=404)

    return JsonResponse(day.serialize(), status=200)

@login_required
def addExercise(request):
    # Creating a new exercise must be via POST
    if request.method != "POST":
        return JsonResponse({"error": "POST request required."}, status=400)
    
    #Load json request
    data = json.loads(request.body)
    day_number = data["dayId"]
    if day_number < 1 : #Day validity check
        return JsonResponse({"error": "Invalid day number"}, status=400)
    name = data["name"]
    set_range = data.get("setRange", "") #Arrays containing min, max value
    rep_range = data.get("repRange", "")

    form = ExerciseForm({"owner": request.user, "name": name, "setRangeMax": set_range[1], "setRangeMin": set_range[0], "repRangeMax": rep_range[1], "repRangeMin": rep_range[0]})
    if form.is_valid():
        try:
            createExercise(request, form, day_number)
        except LookupError as e:
            return JsonResponse({"error": str(e)}, status=400)
        else:
            return JsonResponse({"message": "Exercise created"}, status=201)
    else:
            return JsonResponse({"error": form.errors}, status=400)

@login_required
def retrieveListExercises(request):
    #Verify request method
    if request.method != 'GET':
        return JsonResponse({"error": "GET request required"}, status=400)
    
    #get exercises
    exercises = request.user.exercises.order_by("name").all()
    if not exercises:
        return JsonResponse({"error": "No available exercises"}, status=400)

    return JsonResponse([exercise.serialize() for exercise in exercises], status = 200, safe = False)

@login_required
def addExistingExer(request, day_number, selectedExerId):
    if request.method != "POST":
        return JsonResponse({"error": "POST request required."}, status=400)

    if day_number < 1 : #Day validity check
        return JsonResponse({"error": "Invalid day number"}, status=400)

    day = getOrCreateDay(day_number, request.user)
    try:
        exer = Exercise.objects.get(pk = selectedExerId, owner = request.user)
    except Exercise.DoesNotExist:
        return JsonResponse({"error": "Exercise does not exist"}, status=404)
    
    exercises = day.exercises.all()
    if exer in exercises:
        return JsonResponse({"error": "This exercise is already added"}, status=400)

    exer.instances += 1
    exer.save()
    addExer(day, exer)
    return JsonResponse({"message": "Exercise added"})

@login_required
def changeDayName(request, day_number, day_name):
    #Verify request method
    if request.method != 'POST':
        return JsonResponse({"error": "POST request required"}, status=400)

    if day_number < 1 : #Day validity check
        return JsonResponse({"error": "Invalid day number"}, status=400)

    form = DayForm({"owner": request.user, "number": day_number, "name": day_name})
    if form.is_valid():
        #Get day
        try:
            day = getDay(day_number, request.user)
            day.name = form.cleaned_data['name']
            day.save()
            return JsonResponse({"message": "Name changed"}, status=200)
        except Day.DoesNotExist:
            form.save()
            return JsonResponse({"message": "Created new day with given name"}, status=201)
    else:
        return JsonResponse({"error": form.errors}, status=400)

@login_required
def getExerLogData(request, exerId):
    #Verify request method
    if request.method != 'GET':
        return JsonResponse({"error": "GET request required"}, status=400)

    try: 
        exercise = Exercise.objects.get(pk = exerId)
    except Exercise.DoesNotExist:
        return JsonResponse({"error": "Exercise with specified id does not exist"}, status=404)

    return JsonResponse(exercise.serializeLog(), status=200, safe=False)

@login_required
def addLog(request):
    # Creating a new exercise must be via POST
    if request.method != "POST":
        return JsonResponse({"error": "POST request required."}, status=400)
    
    #Load json request
    log = json.loads(request.body)
    exerId = log["exerId"]
    setNumber = log["sets"]
    repNumber = log["reps"]
    weight = log["weight"]
    info = log["info"]
    try: 
        exercise = Exercise.objects.get(pk = exerId)
    except Exercise.DoesNotExist:
        return JsonResponse({"error": "Exercise with specified id does not exist"}, status=404)

    form = LogForm({"setNumber": setNumber, "repNumber": repNumber, "weight": weight, "info": info})
    if form.is_valid():
        createLog(form, exercise)
        return JsonResponse({"message": "Log created"}, status=201)
    else:
        return JsonResponse({"error": form.errors}, status=400)

def deleteExer(request, day_number ,exerId):
    #Verify request method
    if request.method != 'DELETE':
        return JsonResponse({"error": "DELETE request required"}, status=400)
    try:
        day = getDay(day_number, request.user)
        exercise = Exercise.objects.get(pk = exerId)
    except Day.DoesNotExist:
        return JsonResponse({"error": "Day with specified id does not exist"}, status=404)
    except Exercise.DoesNotExist:
        return JsonResponse({"error": "Exercise with specified id does not exist"}, status=404)

    if exercise.instances == 1:
        exercise.delete()
        day.exercises_count -= 1
        day.save()
        deleteEmptyDay(day)
        return JsonResponse({"message": "Exercise deleted from the database"}, status=200)
    else:
        day.exercises.remove(exercise)
        day.exercises_count -= 1
        day.save()
        exercise.instances -= 1
        exercise.save()
        deleteEmptyDay(day)
        return JsonResponse({"message": "Exercise instance removed"}, status=200) 
    
def login_view(request):
    form = UserLoginForm()

    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(request, username=email, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                messages.info(request, 'Invalid email and/or password')
       
    return render(request, 'Ledger/login.html', {
        'form': form
    })

def register_view(request):
    form = UserRegisterForm()
    
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            passwordRetype = request.POST['passwordRetype']
            password = form.cleaned_data['password']
            if password != passwordRetype:
                messages.info(request, 'Passwords don`t match')
            else:
                email = form.cleaned_data["email"]
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                user = User.objects.create_user(username, email, password)
                user.save()
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
        else:
            messages.info(request, form.errors)
    
    return render(request, 'Ledger/register.html', {
        'form': form
    })

