export function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

export function showErrors(object)
{
    if(typeof object.error === 'object')
    {
        for(const [key, value] of Object.entries(object.error)) //Api returns an object(within field "error") which can have multiple entries 
        {
            console.log(key, value);
            alert(key + ' : ' + value);
        }
    }
    else
    {
        console.log(object.error);
        alert(object.error);
    }
}

export async function request(URL, requestMethod, requestBody) //returns promise
{
    const rmethod = requestMethod.toUpperCase();
    if(rmethod == 'GET') //Depending on the method decide whether to use cookie or not
    {
        var request = URL;
    }
    else
    {
        var request = new Request(
            URL,
            {headers: {'X-CSRFToken': getCookie('csrftoken')}}
        );
    }
     
    var  request_body = {
        method: rmethod,
        body: (requestBody !== undefined) ? JSON.stringify(requestBody) : null
    };

    let response = await fetch(request, request_body);
    const result = await response.json();
    if(!response.ok)
    {
        showErrors(result);
        return false;
    }
    if(result.message !== undefined)
        console.log(result.message);
    return result; 
}