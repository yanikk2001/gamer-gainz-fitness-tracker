import { request } from './request.js';
import {URL} from './urls.js';

var EXERCISE_ID = 1;
var SETS_INPUT = document.querySelector('#track-sets');
var REPS_INPUT = document.querySelector('#track-reps');
var WEIGHT_INPUT = document.querySelector('#track-weight');
var INFO_INPUT = document.querySelector('#track-info');

function ShowTrackInterface()
{
    let trackContainer = document.querySelector('.track-container');
    trackContainer.style.display = 'block';
    trackContainer.style.visibility = 'visible';
    trackContainer.scrollIntoView();
}

function fillTrackTable(logsData) //Recievies array of objects
{
    let tableBody = document.querySelector('.track-table-body');
    tableBody.innerHTML = ''; //Clear old contents
    if(logsData.length == 0)
    {
        tableBody.innerHTML = `<td colspan="4">No track data</td>`;
        return;
    }
    logsData.forEach(log => {
        let tableRow = document.createElement('tr');
        tableRow.className = 'track-table-info';
        let i = 0;
        let numberOfFields = Object.keys(log).length - 1; 
        for(let key in log) //Iterate the fields
        {
            if(i === numberOfFields) //if it is at the final field => create new tr and put content inside it
            {
                if(log[key] !== null)
                {
                    var anotherTableRow = document.createElement('tr');
                    anotherTableRow.innerHTML = `<td colspan="${numberOfFields}">Info: ${log[key]}</td>` //Span all cols
                    break;
                }
                else
                {
                    tableBody.append(tableRow);
                    return;
                }
            }
            else //Create td and put them inside one tr
            {
                i++;
                let tableData = document.createElement('td');
                tableData.innerHTML = log[key];
                tableRow.append(tableData);
            }
        }
        //Puts the 2 new table rows inside the DOM table body
        tableBody.append(tableRow);
        tableBody.append(anotherTableRow);
    });
}

function getTrackData(exerId)
{
    request(URL.GET_EXER_LOG + exerId, 'GET').then(res => {
        if(res)
        {
            fillTrackTable(res.logs);
            
            document.querySelector('.track-popup-name').innerHTML = res.name;
        }
    });
}

export function trackButHandler()
{
    let exerId = this.value;
    EXERCISE_ID = exerId;
    ShowTrackInterface();
    getTrackData(exerId);
}

function clearTrackForm()
{
    SETS_INPUT.value = '';
    REPS_INPUT.value = '';
    WEIGHT_INPUT.value = '';
    INFO_INPUT.value = '';
}

export function HideTrackInterfaceButton_Listener()
{
    document.querySelector('.track-popup-but').addEventListener('click', () => {
        document.querySelector('.track-table-body').innerHTML = '' //Clear old content;
        let trackContainer = document.querySelector('.track-container');
        // trackContainer.style.display = 'none';
        trackContainer.style.visibility = 'hidden'
    });
}

export function trackSubmit_Listener()
{
    document.querySelector('#track-f').onsubmit = () =>
    {
        request(URL.ADD_EXER_LOG, 'POST', {
            exerId: EXERCISE_ID,
            sets: SETS_INPUT.value,
            reps: REPS_INPUT.value,
            weight: WEIGHT_INPUT.value,
            info: INFO_INPUT.value
        }).then(res => {
            if(res)
            {
                getTrackData(EXERCISE_ID);
                clearTrackForm();
            }
        });
        return false;
    }
}