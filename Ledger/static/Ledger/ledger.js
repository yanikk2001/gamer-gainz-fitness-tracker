import {DayManager} from './DayManager.js'
import {show_view} from './ExerciseManager.js'
import {HideTrackInterfaceButton_Listener, trackSubmit_Listener} from './TrackExerLogic.js'

// When DOM loads, render the day
document.addEventListener('DOMContentLoaded', function(){
  //requested(through url) day number 
  const day_num = Number(document.querySelector('#request-day-number').innerHTML);

  let dayManager = new DayManager(day_num);
  document.querySelector('#index-add').addEventListener('click', () => show_view('new-exer-view'));

  document.querySelector('#arrow-left').addEventListener('click', () => {
    dayManager.load_new_day(dayManager.number - 1);
  })

  document.querySelector('#arrow-right').addEventListener('click', () => {
    dayManager.load_new_day(dayManager.number + 1);
  })

  //Touchscreen swipe support for workout view
  var src = document.getElementById('thebody');
  var clientX;
  var screenSize = window.screen.width;

  src.addEventListener('touchstart', function(e) {
    // Cache the client X coordinates
    clientX = e.touches[0].clientX;
  }, false);

  src.addEventListener('touchend', function(e) {
    var deltaX;

    // Compute the change in X coordinates.
    // The first touch point in the changedTouches
    // list is the touch point that was just removed from the surface.
    deltaX = e.changedTouches[0].clientX - clientX;

    if(Math.abs(deltaX) > screenSize/2)
    {
      if(deltaX < 0 )
      {
        dayManager.load_new_day(dayManager.number + 1);
      }
      else
      {
        dayManager.load_new_day(dayManager.number - 1);
      }
    }
  }, false);

  HideTrackInterfaceButton_Listener();
  trackSubmit_Listener();
});

