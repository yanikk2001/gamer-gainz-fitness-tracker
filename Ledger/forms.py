from django.db.models import fields
from django.forms import ModelForm
from .models import Exercise, User, Day, Log
from django import forms
from django.core.exceptions import ValidationError

class UserLoginForm(forms.Form):
    email = forms.EmailField(max_length=60)
    password = forms.CharField(max_length=50)

class UserRegisterForm(ModelForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'password']

class ExerciseForm(ModelForm):
    class Meta:
        model = Exercise
        fields = ['owner' ,'name', 'setRangeMax', 'setRangeMin', 'repRangeMax', 'repRangeMin']

    def clean(self):
        """
        Clean MyModel
        """
        # Get the cleaned data
        cleaned_data = self.cleaned_data

        # Find the unique_together fields
        owner = cleaned_data.get('owner')
        name = cleaned_data.get('name')

        objects = Exercise.objects.filter(owner = owner, name = name)
        if len(objects) > 0:
            msg = u"You have already used this name"
            self._errors["name"] = self.error_class([msg])
            del cleaned_data["name"]
            raise ValidationError(msg)

        return cleaned_data

class DayForm(ModelForm):
    class Meta:
        model = Day
        fields = ['owner', 'number', 'name']

class LogForm(ModelForm):
    class Meta:
        model = Log
        fields = ['setNumber', 'repNumber', 'weight', 'info']