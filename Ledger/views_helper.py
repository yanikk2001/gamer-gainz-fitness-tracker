from Ledger.models import Day, Exercise, Log

def getDay(number, owner):
    return Day.objects.get(number = number, owner = owner)

def getOrCreateDay(number, owner):
    #Check if day exists
    try:
        day = getDay(number, owner)
    except Day.DoesNotExist:
        day = Day(owner = owner,number = number, name = f"Day {number}")
        day.save()
    return day

def addExer(day, exer):
    number = day.exercises_count + 1 #Set ordering of exer in day
    #Update day
    day.exercises.add(exer, through_defaults={'order': number})
    day.exercises_count += 1
    day.save() 

def createExercise(request, form, day_id):
    owner = request.user
    name = form.cleaned_data['name']
    setRangeMax = form.cleaned_data['setRangeMax']
    setRangeMin = form.cleaned_data['setRangeMin']
    repRangeMax = form.cleaned_data['repRangeMax']
    repRangeMin = form.cleaned_data['repRangeMin']

    day = getOrCreateDay(day_id, owner)

    #Create exer
    exercise = Exercise(owner = owner, name = name,
                        setRangeMax = setRangeMax, setRangeMin = setRangeMin, repRangeMax = repRangeMax, repRangeMin = repRangeMin)
    exercise.save()  

    addExer(day, exercise)

def createLog(form, exercise):
    setNumber = form.cleaned_data['setNumber']
    repNumber = form.cleaned_data['repNumber']
    weight = form.cleaned_data['weight']
    info = form.cleaned_data['info']

    log = Log(setNumber=setNumber, repNumber=repNumber, weight=weight,info=info)
    log.save()
    exercise.logs.add(log)
    exercise.save()

def deleteEmptyDay(day):
    if day.exercises_count == 0:
        day.delete()