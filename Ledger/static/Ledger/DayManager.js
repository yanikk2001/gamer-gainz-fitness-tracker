import {ExerciseManager} from './ExerciseManager.js';
import {show_view} from './ExerciseManager.js';
import {request} from './request.js';
import {URL} from './urls.js';

export class DayManager
{
    #exerManager;
    #day_num;
    constructor(number)
    {
        if(!!number) //If no value was provided = false
        {
            this.#day_num = number;
        }
        else
        {
            //Check local storage for last number
            let lastSessionNum = localStorage.getItem("lastSessionDayNumber");
            if(lastSessionNum !== null)
            {
                this.#day_num = Number(lastSessionNum);
            }
            else // Else use default number(1)
            {
                this.#day_num = 1;
            }
        }
        this.#exerManager = new ExerciseManager(this.#day_num);
        this.#show_name_input();
        this.#submit_name_input();
        this.#show_name_text();

        this.#unloadPage_Listener();
    }

    load_new_day(day_num)
    {
        if(day_num > 0)
        {
            this.#day_num = day_num;
            this.#exerManager.load(day_num);
            history.pushState({'day-num': day_num}, '', `/Day/${day_num}`)
        }
    }
   
    get number()
    {
        return this.#day_num;
    }

    #show_name_input()
    {
        document.querySelector('#nav-bar-text').addEventListener('click', function()
        {
            let form = document.querySelector('#form-day-name');
            let input = form.firstElementChild;
            input.value = this.textContent; //text inside nav-bar-text
            show_view(form.id);
        });
    }

    #show_name_text()
    {
        document.addEventListener('click', function(event){
            let isClickInsideElelemnt = document.querySelector('#nav-bar-name').contains(event.target);
            if(!isClickInsideElelemnt)
            {
                show_view('nav-bar-text');
            }
        });
    }

    #submit_name_input()
    {
        document.querySelector('#form-day-name').onsubmit = () => 
        {
            let input = document.querySelector('#input-day-name');
    
            request(URL.DAY + `${this.#day_num}/${input.value}`, 'POST').then(res => {
                if(res)
                {
                    show_view('nav-bar-text');
                    this.load_new_day(this.#day_num);
                }
            });
            return false;
        };
    }

    #unloadPage_Listener()
    {
        window.addEventListener('beforeunload', () => {
            localStorage.setItem("lastSessionDayNumber", this.#day_num);
        });
    }
};