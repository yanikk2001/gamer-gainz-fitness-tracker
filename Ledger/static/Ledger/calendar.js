import { request } from './request.js';
import {URL} from './urls.js';

let counter = 1;
const quantity = 28;
let body = document.querySelector('.cal-body');

document.addEventListener('DOMContentLoaded', function(){

    addWeekTag(0);
    load();

});

function load()
{ 
    let start = counter;
    let end = counter + quantity - 1;
    counter = end + 1;

    request(`/days?start=${start}&end=${end}`, 'GET').then(res => {
        if(res)
        {
            let j = 0;
            for(let i = start; i < counter; i++)
            {
                let day = res.days[j];
                if(day !== undefined && day.number == i)
                {
                    j++;
                }
                else
                {
                    day = undefined;
                }
                
                addDay(day, i);
                
                if(i % 7 === 0)
                {
                    addWeekTag(i);
                }
            }
        }
    });
}

function addWeekTag(i)
{
    let weekTag = document.createElement('div');
    weekTag.className = 'cal-month-tag';
    weekTag.innerHTML = `<h4>Week ${Math.round(i/7)}</h4>`;
    body.append(weekTag);
}

function addDay(day, index)
{
    // Create new day
    const dayCont = document.createElement('a');
    let infocontainer = document.createElement('div');
    let exerNumber = document.createElement('span');
    exerNumber.innerHTML = index;
    
    dayCont.className = 'cal-day boxListLayout';
    dayCont.href = URL.SHOW_DAY + index;
    infocontainer.className = 'cal-day-info';
    exerNumber.className = 'transparentNumber-layout';

    if(day === undefined)
    {
        infocontainer.innerHTML = `<h4 class="grey-text">Day ${index}</h4>`;                        
    }
    else
    {
        infocontainer.innerHTML = `<h4>${day.name}</h4>
                                    <h6>Ex. num: ${day.exercises_count}</h6>`; 
    }
    dayCont.append(infocontainer);
    dayCont.append(exerNumber);

    // Add exercise to DOM
    body.append(dayCont);
}

// If scrolled to bottom, load the next 30 days
window.onscroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        load();
    }
};