import {URL} from './urls.js';
import { request } from './request.js';
import { trackButHandler } from './TrackExerLogic.js';
import {HoldDown} from './HoldDown.js';

export function show_view(viewId)
{
    var container = document.getElementById(viewId).parentElement;
    var views = container.children;
    for(var i = 0; i < views.length; i++)
    {
        if(views[i].id == viewId)
        {
            views[i].style.display = 'block';
        }
        else
        {
            views[i].style.display = 'none';
        }
    }
}

class NewExerciseManager
{
//Private
    #rangeSetMin = document.querySelector('#customRangeSetsMin');
    #rangeSetMax = document.querySelector('#customRangeSetsMax');
    #rangeRepMin = document.querySelector('#customRangeRepsMin');
    #rangeRepMax = document.querySelector('#customRangeRepsMax');
    #maxSetOutput = document.querySelector('#setMax');
    #minSetOutput = document.querySelector('#setMin');
    #maxRepOutput = document.querySelector('#repMax');
    #minRepOutput = document.querySelector('#repMin');

    #exerCheckbox = document.querySelector('#exer-switch');

    #exerMnager;
    
    #clearForm()
    {
        this.#rangeSetMin.value = 1;
        this.#rangeSetMax.value = 1;
        this.#rangeRepMin.value = 1;
        this.#rangeRepMax.value = 1;
        this.#maxSetOutput.innerHTML = '1';
        this.#maxRepOutput.innerHTML = '1';
        this.#minSetOutput.innerHTML = '1';
        this.#minRepOutput.innerHTML = '1';
        document.querySelector('#floatingInput').value = '';
        document.querySelector('#choose-exer-list').value = '';
    }

    #changeRangeMin(rangeInput, rangeOutput, otherRangeInput, otherRangeOutput)
    {
        rangeOutput.innerHTML = rangeInput.value;
        rangeInput.oninput = function() {
            rangeOutput.innerHTML = rangeInput.value;
            otherRangeInput.value = rangeInput.value; //Updates max slider value
            otherRangeOutput.innerHTML = otherRangeInput.value; //Updates max output view
        }
    }
    
    #changeRangeMax(rangeInput, rangeOutput, otherRangeInput)
    {
        rangeOutput.innerHTML = rangeInput.value; //Updates output view on first load
        rangeInput.oninput = function() {
            rangeOutput.innerHTML = rangeInput.value; //Updates output view
            if(parseInt(rangeInput.value) < parseInt(otherRangeInput.value)) //Limits min value of max slider
            {
                rangeInput.disabled = true;
                rangeInput.value = otherRangeInput.value;
                rangeOutput.innerHTML = rangeInput.value;
                rangeInput.disabled = false;
            }
        }
    }

    #backBtn_listener()
    {
        document.querySelector('#back-btn').addEventListener('click', function() {
            show_view('thebody');
        });
    }

    #exer_switch_listener()
    {
        this.#exerCheckbox.addEventListener('change', () => {
          if(this.#exerCheckbox.checked)
          {
            this.hasSelectedChooseOpt = true;
              
            // if checked(option choose exercise) request list of all user exer
            let hasError = false;
            fetch(URL.GET_EXER_LIST)
            .then(response => {
                if(!response.ok)
                {
                    hasError = true;
                }
                return response.json();
            })
            .then(exercises => {
                let exer_layout = document.querySelector('#choose-exer-layout');
                show_view(exer_layout.id);
                if(hasError)
                {
                    console.log(exercises.error);
                    exer_layout.innerHTML = `<h3 class="form-title">${exercises.error}</h3>`;
                    return;
                }
                let dropDownList = document.querySelector('#choose-exer-list');
                dropDownList.innerHTML = '';
                exercises.forEach(exer => {
                    let option = document.createElement('option');
                    option.innerHTML = exer.name;
                    option.value = exer.id;
                    dropDownList.append(option);
                });
            });
            return false;
          }
          else
          {
            this.hasSelectedChooseOpt = false;
            show_view('new-exer-layout');
          }
        });
    }

//Public
    constructor(dayId, exerManager)
    {
        this.id = dayId;
        this.#exerMnager = exerManager;

        //Logic for rangeInput
        this.#changeRangeMin(this.#rangeSetMin, this.#minSetOutput, this.#rangeSetMax, this.#maxSetOutput);
        this.#changeRangeMax(this.#rangeSetMax, this.#maxSetOutput, this.#rangeSetMin);
        this.#changeRangeMin(this.#rangeRepMin, this.#minRepOutput, this.#rangeRepMax, this.#maxRepOutput);
        this.#changeRangeMax(this.#rangeRepMax, this.#maxRepOutput, this.#rangeRepMin);

        this.hasSelectedChooseOpt = this.#exerCheckbox.checked;
        this.#exer_switch_listener();
        this.#exer_onSubmit_Listener();
        this.#backBtn_listener();
    }

    #exer_onSubmit_Listener()
    {
        document.querySelector('#add-exer-form').onsubmit = () => {
            if(!this.hasSelectedChooseOpt)
            {
                    const requestBody = {
                        dayId: this.id,
                        name: document.querySelector('#floatingInput').value,
                        setRange: [this.#rangeSetMin.value, this.#rangeSetMax.value],
                        repRange: [this.#rangeRepMin.value, this.#rangeRepMax.value],
                    };

                    //Returns true if request is successful
                    request(URL.ADD_EXER, 'POST', requestBody).then(res => { 
                            if(res)
                            {
                                this.#exerMnager.load(this.id);
                                this.#clearForm();
                            }
                        });
                    return false;
            }
            else
            {
                    let exerId = document.querySelector('#choose-exer-list').value;
                    request(URL.ADD_EXISTING_EXER + `${this.id}/${exerId}`, 'POST').then(res => {
                        if(res)
                        {
                            this.#exerMnager.load(this.id);
                            this.#clearForm();
                        }
                    });
                    return false;
            }
        };
    }; 
};

export class ExerciseManager
{
    #newExerManager;
    constructor(dayid) //!!!!!day id is not the primary key in the database! It is the number field
    {
        this.#newExerManager = new NewExerciseManager(dayid, this);
        this.load(dayid);
    }

    //PUBLIC
    load(id) 
    {
        this.#removeAllEventListeners(); //Clear old listeners
        this.#newExerManager.id = id; //Update the id of the other class
        show_view('thebody');
        // Get day and it`s posts
        let hasError = false;
        fetch(URL.DAY + id)
        .then(response => {
            if(!response.ok)
            {
                hasError = true;
            }
            if(response.status == 204)
            {
                throw Error("Day does not exist");
            }
            return response.json();
        })
        .then(day => {
            if(hasError)
            {
                console.log(day.error);
                return;
            }
            document.querySelector('#index-rest').style.display = 'none'; //Hide cheems - rest day content
            document.querySelector('#nav-bar-text').innerHTML = day.name;
            document.querySelector('#index-body').innerHTML = ''; //Clear old contents
            let exerNumber = 1;
            day.exercises.forEach(exer => this.#add_exer(exer, exerNumber++));
        })
        .catch(err => {
            document.querySelector('#nav-bar-text').innerHTML = `Day ${id}`;
            document.querySelector('#index-body').innerHTML = '';
            document.querySelector('#index-rest').style.display = 'block'; //Show cheems :D
            console.log(err);
        })
    };

    //Private
    // Add a new exercise with given contents to DOM
    #add_exer(exer, number)
    {
        // Create new exercise
        const post = document.createElement('div');
        post.className = 'index-exer boxListLayout';
        post.style.position = 'relative';
        
        let img = document.createElement('img');
        img.className = 'index-exer-img';
        img.src = URL.ICON_EXER;
        
        let infocontainer = document.createElement('div');
        infocontainer.className = 'index-exer-info';
        infocontainer.innerHTML = `<h4>${exer.name}</h4>
                                <h6>Set range: ${exer.setNumberRange[0]} - ${exer.setNumberRange[1]}</h6>
                                <h6>Rep range: ${exer.repNumberRange[0]} - ${exer.repNumberRange[1]}</h6>`
        
        let deleteBut = document.createElement('button');
        deleteBut.className = 'exer-close-but close-but';
        deleteBut.innerHTML = '<img style="position: relative;"src="/media/assets/x-lg.svg" alt="close"></img>';
        deleteBut.style.position = 'absolute';
        deleteBut.style.top = '-1px';
        deleteBut.style.right = '-1px';
        deleteBut.style.display = 'none';
        deleteBut.addEventListener('click', () => {
            let url = URL.DELETE_EXER + this.#newExerManager.id + '/' + exer.id;
            request(url, 'DELETE').then(res => {
                if(res)
                {
                    this.load(this.#newExerManager.id);
                }
            })
            
        });

        let exerNumber = document.createElement('span');
        exerNumber.className = 'index-exer-number transparentNumber-layout';
        exerNumber.innerHTML = number;

        let trackbut = document.createElement('button');
        trackbut.className = 'index-exer-but';
        trackbut.innerHTML = 'TRACK';
        trackbut.value = exer.id;
        trackbut.addEventListener('click', trackButHandler); //The func has to be unanonymous so the listener can be removed

        post.append(img);
        post.append(infocontainer);
        post.append(deleteBut);
        post.append(exerNumber);
        post.append(trackbut);

        // Add exercise to DOM
        document.querySelector('#index-body').append(post);
        HoldDown.add_Listener(post);
    };

    #removeAllEventListeners()
    {
        let buttons = document.querySelectorAll('.index-exer-but');
        buttons.forEach(but => but.removeEventListener('click', trackButHandler));
    }
}
