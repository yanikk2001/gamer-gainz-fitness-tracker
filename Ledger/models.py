from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator, validate_comma_separated_integer_list
from django.db.models.fields import CharField
from datetime import date

class MyAccountManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not username:
            raise ValueError("Users must have an username")
        if not password:
            raise ValueError("Enter password")
        user = self.model(
            email = self.normalize_email(email),
            username = username,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            email=self.normalize_email(email),
            username = username,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

# def get_user_profile_image(self, filepath):
#     return f'user_profiles/{self.id}/user_image.png'
# def get_user_default_profile_image():
#     return 'def_user_profile_img/def_img.png'

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True, null=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    hide_email = models.BooleanField(default=True)
    # image = models.ImageField(max_length = 255, upload_to=get_user_profile_image, null= True, blank=True, default=get_user_default_profile_image)

    objects = MyAccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

class Log(models.Model):
    date = models.DateField(default=date.today)
    setNumber = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    repNumber = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(999)])
    weight = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(999)])
    info = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return f"Log on {self.date}"

    def serialize(self):
        return {
            "date": self.date,
            "setNumber": self.setNumber,
            "repNumber": self.repNumber,
            "weight": self.weight,
            "info": self.info
        }

class Exercise(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="exercises")
    name = models.CharField(max_length=50)
    setRangeMax = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(99)])
    setRangeMin = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(99)])
    repRangeMax = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(250)])
    repRangeMin = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(250)])
    logs = models.ManyToManyField(Log, related_name='exercise', blank=True)
    instances = models.IntegerField(default= 1)

    class Meta:
        unique_together = ['owner', 'name']

    def __str__(self):
        return self.name

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "setNumberRange": [self.setRangeMin, self.setRangeMax],
            "repNumberRange": [self.repRangeMin, self.repRangeMax]
        }

    def serializeLog(self):
        return {
            "id": self.id,
            "name": self.name,
            "logs": [log.serialize() for log in self.logs.all()],
        }

class Day(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="days")
    number = models.IntegerField(default=1)
    name = CharField(max_length=25)
    exercises = models.ManyToManyField(Exercise, related_name='day', blank=True, through='Ordering')
    exercises_count = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(99)])

    class Meta:
        ordering = ['number']

    def serialize(self):
        return {
            "owner": self.id,
            "number": self.number,
            "name": self.name,
            "exercises": [exercise.serialize() for exercise in self.exercises.all()],
            "exercises_count": self.exercises_count
        }
    def calSerialize(self):
        return {
            "number": self.number,
            "name": self.name,
            "exercises_count": self.exercises_count
        }

class Ordering(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    order = models.IntegerField(default=1)

    