from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from Ledger.models import *

class AccountAdmin(UserAdmin):
    list_display = ('email', 'username', 'date_joined', 'last_login', 'is_admin', 'is_staff')
    search_fields = ('email', 'username')
    readonly_fields = ('id', 'date_joined', 'last_login')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(User,AccountAdmin)

class ExerciseAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner')
    search_fields = ('name',)
    readonly_fields = ('id',)

admin.site.register(Exercise, ExerciseAdmin)

class LogAdmin(admin.ModelAdmin):
    list_display = ('date', 'setNumber', 'repNumber', 'weight')
    list_filter = ('date', 'exercise')
    readonly_fields = ('id',)

admin.site.register(Log, LogAdmin)

class DayAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'exercises_count', 'id')
    search_fields = ('owner',)
    readonly_fields = ('id',)

admin.site.register(Day, DayAdmin)

