from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("calendar", views.calendarView, name="calendar"),
    path("login", views.login_view, name="login"),
    path("register", views.register_view, name="register"),
    path("Day/<int:day_number>", views.day, name="Day"),
    path("profile", views.profileView, name="profile"),
    path("logout", views.logout_view, name="logout"),

    # API Routes
    path("day/<int:day_number>", views.dayApi, name="dayApi"),
    #Creates new exer and adds it to a day if it exists or creates new one (saves the need for additional requests)
    path("add-exercise", views.addExercise, name="add-exercise"), 
    path("add-existing-exercise/<int:day_number>/<int:selectedExerId>", views.addExistingExer, name="add-existing-exercise"), 
    path("list-exercises", views.retrieveListExercises, name="list-exercises"), 
    path("day/<int:day_number>/<str:day_name>", views.changeDayName, name="change-day-name"),
    path("exer-log/<int:exerId>", views.getExerLogData, name="exer-log"),
    path("add-log", views.addLog, name="add-log"),
    path("days", views.getDays, name="days"),
    path("delete-exercise/<int:day_number>/<int:exerId>", views.deleteExer, name="delete-exercise"),
]